export default (app) => {
  const cid = app.$cookies.get('headerApi.cid')
  if (cid) {
    app.$axios.defaults.headers.common['X-Cid'] = cid
  }
  app.$axios.defaults.headers.common['Content-Type'] = 'application/json'
}
