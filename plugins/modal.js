import Vue from 'vue'
import Modal from 'vue-js-modal'

Vue.use(Modal, {
  dynamicDefaults: {
    // shiftY: 0,
    // adaptive: true,
    // draggable: false,
    // resizable: false,
    scrollable: true,
    height: 'auto',
    width: '100%'
    // transition: 'modal'
  }
})
