export default {
  target: 'static',
  router: {
    base: '/promo/riomajor/'
  },
  generate: {
    crawler: false
  },

  head: {
    title: 'iem-rio',
    htmlAttrs: {
      lang: 'ru'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
      { name: 'msapplication-TileColor', content: '#da532c' },
      { name: 'theme-color', content: '#ffffff' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/promo/riomajor/favicon.ico' },
      { rel: 'apple-touch-icon', sizes: '180x180', href: '/promo/riomajor/apple-touch-icon.png' },
      { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/promo/riomajor/favicon-32x32.png' },
      { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/promo/riomajor/favicon-16x16.png' },
      { rel: 'manifest', href: '/promo/riomajor/site.webmanifest' },
      { rel: 'preconnect', href: 'https://fonts.googleapis.com' },
      { rel: 'preconnect', href: 'https://fonts.gstatic.com', 'crossorigin': '' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;700;800&display=swap' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Caveat:wght@700&display=swap' },
      { rel: 'stylesheet', href: '/promo/riomajor/roulette/vanillaRoulette.min.css' }
    ],
    script: [
      { src: '/promo/riomajor/roulette/vanillaRoulette.min.js' }
    ]
  },

  css: [
    '~/assets/styles/normalize.less',
    '~/assets/styles/global.less'
  ],

  plugins: [
    { src: '~/plugins/axios.js', mode: 'client' },
    { src: '~/plugins/modal.js', mode: 'client' },
    { src: '~/plugins/device.js', mode: 'client' },
    { src: '~/plugins/tooltip.js', mode: 'client' },
    { src: '~/plugins/swiper.js', mode: 'client' }
  ],

  buildModules: [
  ],

  modules: [
    'cookie-universal-nuxt',
    '@nuxtjs/axios',
    '@nuxtjs/gtm',
  ],
  gtm: {
    enabled: true,
    id: 'GTM-MNL6C3X', // Used as fallback if no runtime config is provided
  },

  axios: {
    baseURL: 'https://api.rio.sportbonus.team'
  },

  build: {
  }
}
