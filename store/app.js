export const state = () => ({
  day: null,
  blocks: null,
  identity: null,
  cases: null,
  settings: null,
  theCase: null
})

export const getters = {}

export const actions = {
  async getDay({ commit }) {
    try {
      const day = await this.$axios.$get('/day?expand=matches.teamA,matches.teamB')
      commit('data', {
        field: 'day',
        data: day
      })
    } catch (e) {}
  },
  async postForecast({ commit },  payload) {
    try {
      await this.$axios.$post('/day/forecast', payload)
    } catch (e) {
      console.log(e.response.data.message)
    }
  },
  async getBlocks({ commit }) {
    try {
      const blocks = await this.$axios.get('/blocks')
      commit('data', {
        field: 'blocks',
        data: blocks.data
      })
    } catch (e) {}
  },
  async getIdentity({ commit }) {
    try {
      // console.log(this.$cookies.get('headerApi.cid'))
      // console.log(this.$axios.defaults.headers.common)
      const identity = await this.$axios.$get('/auth/identity')
      commit('data', {
        field: 'identity',
        data: identity
      })
    } catch (e) {
      if (e.response.status === 401) {
        console.log(e.response.data.name)
      }
    }
  },
  async getSettings({ commit }) {
    try {
      const settings = await this.$axios.$get('/settings')
      commit('data', {
        field: 'settings',
        data: settings
      })
    } catch (e) {}
  },
  async getCases({ commit }) {
    try {
      const cases = await this.$axios.$get('/cases?sort=price')
      commit('data', {
        field: 'cases',
        data: cases
      })
    } catch (e) {}
  },
  async getCase({ commit }, id) {
    try {
      const theCase = await this.$axios.$get(`/cases/${id}?expand=items`)
      commit('data', {
        field: 'theCase',
        data: theCase
      })
    } catch (e) {}
  },
  async openCase(store, id) {
    let res
    try {
      res = await this.$axios.$post(`/cases/${id}/open`)
    } catch (e) {}
    return res
  }
}

export const mutations = {
  data(state, { field, data }) {
    state[field] = data
  }
}
